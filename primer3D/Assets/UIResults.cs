using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIResults : MonoBehaviour
{
    public GameObject Win;
    public Text textCoins;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
        textCoins.text = GameManager.Instance.coins.ToString();
    }

    void CheckWin()
    {
        if (GameManager.Instance.coins >= GameManager.Instance.coinsMap)
        {
            Win.SetActive(true);
        }
    }
   public void PlayAgainButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Playground");
    }
}
