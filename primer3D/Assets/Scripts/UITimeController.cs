using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UITimeController : MonoBehaviour
{
    public int min, seg;
    public Text time;

    private bool _isRunningTimer;
    private float _res;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(true);
        _res = (min * 60) + seg;
        _isRunningTimer = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isRunningTimer)
        {
            _res -= Time.deltaTime;
            if (_res < 1)
            {
                _isRunningTimer = false;
                GameManager.Instance.AtivateResults();
                this.gameObject.SetActive(false);
            }
            int tempMin = Mathf.FloorToInt(_res / 60);
            int tempSeg = Mathf.FloorToInt(_res % 60);
            time.text = string.Format("{00:00}:{01:00}", tempMin, tempSeg);
        }
    }
}