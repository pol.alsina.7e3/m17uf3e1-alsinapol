using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDDCoins : MonoBehaviour
{
    private ColliderCoins _colliderCoins;
    public Text textCoins;
    private void Awake()
    {
        ColliderCoins.coinUp += CoinPlus1Text;
    }
    void CoinPlus1Text()
    {
        textCoins.text = GameManager.Instance.coins.ToString();
        //this.transform.childCount;
    }
}
