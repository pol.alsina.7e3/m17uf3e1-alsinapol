using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderCoins : MonoBehaviour
{
    public delegate void CoinUp();
    public static event CoinUp coinUp;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "coin")
        {
            GameManager.Instance.coins++;
            GameManager.Instance.CheckCoins();
            Debug.Log(GameManager.Instance.coins);
            if (coinUp != null) coinUp();                    
            Destroy(other.gameObject);
        }
    }
}
