using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public int coins;
    public int coinsMap;
    public GameObject results;
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    public void CheckCoins()
    {
        if (coins >= coinsMap)
        {
            AtivateResults();
        }
    }
    public void AtivateResults()
    {       
        results.SetActive(true);
    }
    
}
